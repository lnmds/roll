#!/usr/bin/python

import collections
import operator
from random import SystemRandom
random = SystemRandom()

PLAYER_SIZE = 2**15
INITIAL_GOLD = [39]

class Player:
    def __init__(self, gold):
        self.gold = gold
        self.rolls = 0
        self.days = 0
        self.actions = []

    def __str__(self):
        return 'G[%s] %s' % (self.gold, self.actions)

    def get_10roll(self):
        self.actions.append('10roll')
        self.gold -= 30
        for i in range(10):
            self.roleta()

    def roleta(self):
        self.actions.append('roll')
        self.rolls -= 1
        if random.random() < 0.05:
            self.actions.append('GOTIT')
        else:
            self.actions.append('DESAPONTANTE')

    def wait_day(self):
        self.actions.append("WAITDAY")
        self.days += 1
        self.gold += 10

    def make_actions(self):
        for i in range(5):
            action_list = ['waitday', 'roll', '10roll']
            action = random.choice(action_list)
            if action == 'waitday':
                self.wait_day()
            elif action == 'roll':
                self.roleta()
            elif action == '10roll':
                self.get_10roll()
            self.check_sanity()

    def check_sanity(self):
        if self.gold < 0:
            self.actions.append('NOGOLD')

        if self.days > 6:
            self.actions.append('EVENT_END')

    def result(self):
        if len(self.actions) > 0:
            return self.actions[len(self.actions)-1] == 'GOTIT'
        else:
            return False

def main():
    good_results = {}
    while True:
        players = []

        for i in range(PLAYER_SIZE):
            players.append(Player(random.choice(INITIAL_GOLD)))

        for player in players:
            player.make_actions()
            if player.result():
                player_string = str(player)
                if player_string not in good_results:
                    good_results[str(player)] = 0
                good_results[str(player)] += 1

        gresults_ordered = sorted(good_results, key=good_results.get)

        for actionstring in gresults_ordered:
            val = good_results[actionstring]
            if val > 10:
                print(val, actionstring)

        print("="*50)



if __name__ == '__main__':
    main()
